import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import serenitySteps.LoginSteps;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src\\test\\java\\resource\\features")
public class RunTestSuite {


}
