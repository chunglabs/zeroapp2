Feature: Login
  As a user
  I want to buy some ZB
  So that I need to login Zero Bank App

  Scenario Outline: Login with invalid data
    Given The user opened Zerobank Application
    When He choose his "<country>"
    And He fill "<phone>" and "<password>" to login
    Then He should see "<message>"


    Examples:
      | country | phone | password | message |
      | Vietnam     | 94331fsdf     | 123456789aA | Username or password is incorrect. Please try again |
      | Vietnam     | 943313848     | 123456789aA | Username or password is incorrect. Please try again |
      | Vietnam     |      | 123456789aA | Username or password is incorrect. Please try agai |
      | Vietnam     |   943313848   |  | Username or p |
