package pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class LoginPage extends MobilePageObject {
    public LoginPage(WebDriver driver) {
        super(driver);
    }
    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"txtPhone\"]")
    WebElement txtPhone;

    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"txtPassword\"]")
    WebElement txtPassword;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"btnLogin\"]")
    WebElement btnLogin;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"txtContentAlert\"]" )
    public WebElement txtContentAlert;

    public void inputPhone(String phone){
        txtPhone.sendKeys(phone);
    }
    public void inputPassword(String password){
        txtPassword.sendKeys(password);
    }
    public void clickLogin(){
        btnLogin.click();
    }

    public void verifyAlert(String message){
        Assert.assertEquals(message,txtContentAlert.getText());
    }

}
