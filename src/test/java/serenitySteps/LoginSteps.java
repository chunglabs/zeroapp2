package serenitySteps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import pages.LoginPage;

public class LoginSteps extends ScenarioSteps {
    LoginPage loginPage;
    @Step
    public void login_with_phone_and_password(String phone, String password){

        loginPage.inputPhone(phone);
        loginPage.inputPassword(password);
        loginPage.clickLogin();
    }
    @Step
    public void verify_alert(String message){
        loginPage.verifyAlert(message);
    }

}
