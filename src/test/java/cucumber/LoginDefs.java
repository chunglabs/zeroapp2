package cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.support.ui.WebDriverWait;
import serenitySteps.LoginSteps;

public class LoginDefs {
    @Steps
    LoginSteps loginSteps;
    @Given("^The user opened Zerobank Application$")
    public void theUserOpenedZerobankApplication() {

        System.out.println("open app");
    }

    @When("^He choose his \"([^\"]*)\"$")
    public void heChooseHisCountry(String country) throws Throwable {
        System.out.println(country);
        throw new PendingException();
    }
    @And("^He fill \"([^\"]*)\" and \"([^\"]*)\" to login$")
    public void heFillPhoneAndPasswordToLogin(String phone, String password) throws Throwable {
        loginSteps.login_with_phone_and_password(phone, password);
    }
    @Then("^He should see \"([^\"]*)\"$")
    public void heShouldSeeMessage(String message) throws Throwable {
        loginSteps.verify_alert(message);

    }


}
